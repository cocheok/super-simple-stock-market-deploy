var template = (function () {
    var db;
    var pageTemplate = 'page_template';
    var connectionInfo = 'localhost:27017';
    var dbName = 'dsapidb';

    var makeId = function() {
        var a = Math.random().toString(36).substring(6);
        return a;
    };

    var templates = [
        {
            templateName: 'Una regi\u00F3n (base)',
            description: 'Plantilla con una sola regi\u00F3n',
            url: '/base-edit',
            viewUrl: '/base/',
            regions: [
                {
                    name: 'content',
                    order: 1,
                    id: makeId(),
                    render: true
                }
            ]
        },
        {
            templateName: 'Dos regi\u00F3nes (HC)',
            description: 'Plantilla con dos regi\u00F3n. Un encabezado y un contenido',
            url: '/hc-edit',
            viewUrl: '/hc/',
            regions: [
                {
                    name: 'header',
                    order: 1,
                    id: makeId(),
                    render: true
                },
                {
                    name: 'content',
                    order: 2,
                    id: makeId(),
                    render: true
                }
            ]
        },
        {
            templateName: 'Tres regi\u00F3nes (HCC)',
            description: 'Plantilla con tres regi\u00F3n. Un encabezado y cuerpo dividio en dos regiones',
            url: '/hcc-edit',
            viewUrl: '/hcc/',
            regions: [
                {
                    name: 'header',
                    order: 1,
                    id: makeId(),
                    render: true
                },
                {
                    name: 'content',
                    order: 2,
                    id: makeId(),
                    render: false
                },
                {
                    name: 'left',
                    order: 3,
                    parent: 2,
                    id: makeId(),
                    render: true
                },
                {
                    name: 'right',
                    order: 4,
                    parent: 2,
                    id: makeId(),
                    render: true
                }
            ]
        },
        {
            templateName: 'Cuatro regi\u00F3nes (HCCF)',
            description: 'Plantilla con cuatro regi\u00F3n. Un encabezado con cuerpo dividio en dos regiones y un pi\u00E9 de p\u00E1gina',
            url: '/hccf-edit',
            viewUrl: '/hccf/',
            regions: [
                {
                    name: 'header',
                    order: 1,
                    id: makeId(),
                    render: true
                },
                {
                    name: 'content',
                    order: 2,
                    id: makeId(),
                    render: false
                },
                {
                    name: 'left',
                    order: 3,
                    parent: 2,
                    id: makeId(),
                    render: true
                },
                {
                    name: 'right',
                    order: 4,
                    parent: 2,
                    id: makeId(),
                    render: true
                },
                {
                    name: 'footer',
                    order: 5,
                    id: makeId(),
                    render: true
                }
            ]
        }
    ];

    var printMsg = function (msg) {
        print(msg);
    };

    var close = function () {
        quit();
    };

    var startConnection = function () {
        if(!db) {
            printMsg('connecting to massimple db');
            db = new Mongo(connectionInfo).getDB(dbName);
            printMsg('db connection established');
        }
    };

    var dropTemplateCollection = function() {
        printMsg('Deleting existent collection');
        db[pageTemplate].drop();
    };

    var templateInitialization = function () {
        printMsg('Starting template creation process');
        startConnection();
        dropTemplateCollection();
        printMsg('Creating  template\'s page information');

        templates.forEach(function (template){
            printMsg('Saving template ' + template.templateName);
            db[pageTemplate].save(template)
        });

        printMsg('Process completed');
        close();

    };

    return {
        initTemplates : templateInitialization,
        printMsg : printMsg
    };

})();

if(typeof db !== 'undefined') {
    template.printMsg('Template creation initialization');
    template.initTemplates();
};
