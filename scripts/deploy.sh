#!/bin/bash
docker login -u $1 -p $2 $3
docker-compose -f ../docker-compose.yml down
docker-compose -f ../docker-compose.yml pull 
docker logout $3
docker-compose -f ../docker-compose.yml up -d
